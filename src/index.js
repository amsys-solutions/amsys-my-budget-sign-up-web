import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './components/App';

import store from './redux/store';

function Root(props) {
  return (
      <Provider store={store}>
          <App />
      </Provider>
  );
}

ReactDOM.render(<Root />,  document.getElementById('root'));
