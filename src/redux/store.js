import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import commons from './reducers/common';
import users from './reducers/users';

const reducer = combineReducers({
    commons,
    users
});

const store = createStore(reducer, applyMiddleware(thunk));

export default store;