import axios from 'axios';

import {
    LOGIN_USER,
    GENERIC_WAITING_FOR,
    GENERIC_WAITING_IS_OVER,
    GENERIC_ERROR
} from '../types/Types';

import {
    API_LOGIN_USER,
    LINK_APPROVED_LOGIN
} from '../url/Url'

const LoginUser = (user) => {
    return dispatch => {
        // mostramos el cargando
        dispatch(waitingFor());
        //
        var raw = JSON.stringify(user);
        var requestOptions = {
            method: 'POST',
            body: raw,
            redirect: 'follow'
        };
        //
        fetch(API_LOGIN_USER, requestOptions)
            .then(response => response.json())
            .then((response) => {
                dispatch(login({ "in": response.approveLogin, "redirect": LINK_APPROVED_LOGIN }));
                dispatch(waitingIsOver());
            })
            .catch((error) => {
                dispatch(throwError(error));
                dispatch(waitingIsOver());
            });
    };
};

const login = data => ({
    type: LOGIN_USER,
    payload: data
});

const throwError = error => ({
    type: GENERIC_ERROR,
    payload: {
        error
    }
});

const waitingFor = () => ({
    type: GENERIC_WAITING_FOR
});

const waitingIsOver = () => ({
    type: GENERIC_WAITING_IS_OVER
});

export default LoginUser;