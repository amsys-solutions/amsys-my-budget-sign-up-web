import { 
    LOGIN_USER
} from '../types/Types';

const defaultState = {
    login: false
}

const reducer = (state = defaultState, { type, payload }) => {
    switch (type) {
        case LOGIN_USER: {
            state.login = payload.in === true;
            state.redirect = payload.redirect;
            break;
        }
        default:
            return state;
    }
    //
    return state;
};

export default reducer;