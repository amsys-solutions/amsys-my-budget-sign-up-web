import { 
    GENERIC_WAITING_FOR, 
    GENERIC_WAITING_IS_OVER, 
    GENERIC_VIEW_MENU,
    GENERIC_ERROR 
} from '../types/Types';

const defaultState = {
    waitingFor: false,
    viewMenu: false
}

const reducer = (state = defaultState, { type, payload }) => {

    switch (type) {
        case GENERIC_WAITING_FOR: {
            state.waitingFor = true;
            break;
        }

        case GENERIC_WAITING_IS_OVER: {
            state.waitingFor = false;
            break;
        }

        case GENERIC_VIEW_MENU : {
            state.viewMenu = payload;
            break;
        }
        
        case GENERIC_ERROR : {
            console.log(payload);
            break;
        }

        default:
            return state;
    }
    //
    return state;
};

export default reducer;