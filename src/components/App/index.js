import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import Login from '../Login';
import Loading from '../Loading';
import Redirect from '../Redirect';

import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  }
}));


export default function App(props) {
  const classes = useStyles();
  const login = useSelector(store => store.users.login);
  //
  let display = <div></div>;
  //
  if (login === false) {
    display = (
      <div>
        <Loading />
        <Login />
      </div>
    );
  }
  else if (login === true){
    display = (
      <div className={classes.root}>
        <Redirect />
      </div>
    );
  }
  //
  return display;
}