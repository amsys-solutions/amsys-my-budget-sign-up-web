import React, {useEffect} from 'react';
import { useSelector } from 'react-redux';

import useStyles from './styles';

const Redirect = () => {
  useStyles();
  //
  const redirect = useSelector(store => store.users.redirect);
  //
  useEffect(() => {
    //window.location.href = redirect;
    console.log("Redirect: " + redirect);
  }, []);

  return (
    <div>Redirigiendo...</div>
  );
};

export default Redirect;