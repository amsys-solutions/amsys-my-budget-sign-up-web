import React, { useEffect } from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import useStyles from './styles';
import { useSelector } from 'react-redux';

import { v4 as uuidv4 } from 'uuid';

const uuid = uuidv4();

const Loading = () => {
  const classes = useStyles();
  const waitingFor = useSelector(store => store.commons.waitingFor);

  return (
    <div>
      <Backdrop className={classes.backdrop} open={waitingFor} >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}

export default Loading;