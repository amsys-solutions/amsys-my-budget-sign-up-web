import { makeStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
});

const useStyles = makeStyles(styles);

export default useStyles;